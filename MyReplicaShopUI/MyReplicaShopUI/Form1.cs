﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace MyReplicaShopUI
{
    public partial class Form1 : Form
    {
        SqlDataAdapter daCustomer, daOrders, daOrdered, daStorage, daHist, daHistStore;
        DataSet dsCustomer, dsOrders, dsOrdered, dsStorage, dsHist, dsHistStore;
        BindingSource bsCustomer, bsOrders, bsOrdered, bsStorage, bsHist, bsHistStore;
        SqlConnection connection;
        SqlCommandBuilder cmdbl;
        DataTable dtDropDown, dtDropStore;
        SqlCommand command;

        public Form1()
        {
            InitializeComponent();
            connection = new SqlConnection("Data Source=RECEPTION\\SQLEXPRESS02;Initial Catalog=myReplicaShop;Integrated Security=True");
            connection.Open();
            if (connection.State != ConnectionState.Open) { MessageBox.Show("Connction error!"); }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadCustomers();
            loadStorage();
            loadOrders();
            loadDropDownHist();
            loadDropDownStor();
            loadOrderedProducts();
        }

        private void loadCustomers()
        {

            daCustomer = new SqlDataAdapter("Select * from CUSTOMER", connection);
            dsCustomer = new DataSet();
            daCustomer.Fill(dsCustomer, "CUSTOMER");
            bsCustomer = new BindingSource();
            bsCustomer.DataSource = dsCustomer.Tables[0].DefaultView;

            textBox1.DataBindings.Add(new Binding("Text", bsCustomer, "Surname", true));
            textBox2.DataBindings.Add(new Binding("Text", bsCustomer, "Name", true));
            textBox3.DataBindings.Add(new Binding("Text", bsCustomer, "DateOfBirth", true));
            textBox4.DataBindings.Add(new Binding("Text", bsCustomer, "Age", true));
            textBox5.DataBindings.Add(new Binding("Text", bsCustomer, "City", true));
            textBox6.DataBindings.Add(new Binding("Text", bsCustomer, "Adress", true));
            textBox7.DataBindings.Add(new Binding("Text", bsCustomer, "CustomerCode", true));
            textBox8.DataBindings.Add(new Binding("Text", bsCustomer, "VAT", true));
            textBox9.DataBindings.Add(new Binding("Text", bsCustomer, "TaxOffice", true));
            textBox10.DataBindings.Add(new Binding("Text", bsCustomer, "PhoneNumber", true));
            textBox11.DataBindings.Add(new Binding("Text", bsCustomer, "Comments", true));
            bindingNavigator1.BindingSource = bsCustomer;
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            cmdbl = new SqlCommandBuilder(daCustomer);
            daCustomer.Update(dsCustomer, "CUSTOMER");
            MessageBox.Show("Customers updated!");
        }



        private void loadStorage()
        {
            daStorage = new SqlDataAdapter("Select * from Storage", connection);
            dsStorage = new DataSet();
            daStorage.Fill(dsStorage, "Storage");
            bsStorage = new BindingSource();
            bsStorage.DataSource = dsStorage.Tables[0].DefaultView;
            textBox12.DataBindings.Add(new Binding("Text", bsStorage, "ProductCode", true));
            textBox13.DataBindings.Add(new Binding("Text", bsStorage, "Kind", true));
            textBox14.DataBindings.Add(new Binding("Text", bsStorage, "Category", true));
            textBox15.DataBindings.Add(new Binding("Text", bsStorage, "Reserve", true));
            textBox16.DataBindings.Add(new Binding("Text", bsStorage, "Price", true));
            textBox17.DataBindings.Add(new Binding("Text", bsStorage, "Tax", true));
            bindingNavigator2.BindingSource = bsStorage;
        }

        private void saveToolStripButton1_Click(object sender, EventArgs e)
        {
            cmdbl = new SqlCommandBuilder(daStorage);
            daStorage.Update(dsStorage, "Storage");
            MessageBox.Show("Storage updated!");
        }

        private void loadOrders()
        {
            daOrders = new SqlDataAdapter("Select * from Orders", connection);
            dsOrders = new DataSet();
            daOrders.Fill(dsOrders, "Orders");
            bsOrders = new BindingSource();
            bsOrders.DataSource = dsOrders.Tables[0].DefaultView;
            textBox18.DataBindings.Add(new Binding("Text", bsOrders, "OrderID", true));
            textBox20.DataBindings.Add(new Binding("Text", bsOrders, "CustomerCode", true));
            textBox21.DataBindings.Add(new Binding("Text", bsOrders, "Payment", true));
            textBox22.DataBindings.Add(new Binding("Text", bsOrders, "PlaceOfDelivery", true));
            bindingNavigator3.BindingSource = bsOrders;
        }

        private void saveToolStripButton2_Click(object sender, EventArgs e)
        {
            cmdbl = new SqlCommandBuilder(daOrders);
            daOrders.Update(dsOrders, "Orders");
            MessageBox.Show("Orders updated!");
        }


        private void loadDropDownHist()
        {
            daCustomer = new SqlDataAdapter("Select * from CUSTOMER", connection);
            dtDropDown = new DataTable();
            daCustomer.Fill(dtDropDown);
            comboBox1.DataSource = dtDropDown;
            comboBox1.DisplayMember = "Surname";
        }

        public void fillHistoryDataset()
        {
            daOrdered = new SqlDataAdapter("Select Surname,Name,OrderedProducts.OrderID,Kind,Quantity,Price,Tax from" +
            " CUSTOMER inner join (Orders inner join (OrderedProducts inner join Storage on Storage.ProductCode = OrderedProducts.ProductCode)" +
            " on OrderedProducts.OrderID = Orders.OrderID) on CUSTOMER.CustomerCode = Orders.CustomerCode" +
            " where Surname = '" + comboBox1.Text.ToString() + "'", connection);
            dsOrdered = new DataSet();
            daOrdered.Fill(dsOrdered);
            bsOrdered = new BindingSource();
            bsOrdered.DataSource = dsOrdered.Tables[0].DefaultView;
            dataGridView1.DataSource = bsOrdered;
            float price = 0;
            int quantity = 0;
            float sum = 0;
            float tax = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                price = Convert.ToSingle(dataGridView1.Rows[i].Cells[5].Value);
                quantity = Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value);
                tax = Convert.ToSingle(dataGridView1.Rows[i].Cells[6].Value);
                sum += quantity * (price + (price * tax));
            }

            label28.Text = sum.ToString();
        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillHistoryDataset();
        }

        private void loadOrderedProducts()
        {
            daHist = new SqlDataAdapter("Select Surname,OrderedProducts.OrderID,OrderedProducts.ProductCode,Kind,Quantity from" +
            " CUSTOMER inner join (Orders inner join (OrderedProducts inner join Storage on Storage.ProductCode = OrderedProducts.ProductCode)" +
            " on OrderedProducts.OrderID = Orders.OrderID) on CUSTOMER.CustomerCode = Orders.CustomerCode Order by Surname", connection);
            dsHist = new DataSet();
            daHist.Fill(dsHist, "OrderedProducs");
            bsHist = new BindingSource();
            bsHist.DataSource = dsHist.Tables[0].DefaultView;
            dataGridView2.DataSource = bsHist;
            bindingNavigator4.BindingSource = bsHist;
        }

        private void saveToolStripButton3_Click(object sender, EventArgs e)
        {
            cmdbl = new SqlCommandBuilder(daHist);
            daHist.Update(dsHist, "OrderedProducts");
            MessageBox.Show("Ordered products updated!");
        }

        private void loadDropDownStor()
        {
            daStorage = new SqlDataAdapter("Select * from Storage", connection);
            dtDropStore = new DataTable();
            daStorage.Fill(dtDropStore);
            comboBox2.DataSource = dtDropStore;
            comboBox2.DisplayMember = "Kind";
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillProductHistory();
        }

        private void fillProductHistory()
        {
            daHistStore = new SqlDataAdapter("Select Storage.ProductCode, Kind, Category, Reserve,(Select SUM(Quantity) From OrderedProducts inner join "+
                "Storage on Storage.ProductCode = OrderedProducts.ProductCode where Kind = '" + comboBox2.Text.ToString() + "'" +") As Ordered, Price, Tax from Storage "+
                "where Kind = '" + comboBox2.Text.ToString() + "'", connection);
            dsHistStore = new DataSet();
            daHistStore.Fill(dsHistStore);
            bsHistStore = new BindingSource();
            bsHistStore.DataSource = dsHistStore.Tables[0].DefaultView;
            dataGridView3.DataSource = bsHistStore;
            float price = 0;
            int quantity = 0;
            float sum = 0;
            float tax = 0;
            for (int i = 0; i < dataGridView3.Rows.Count; i++)
            {
                price = Convert.ToSingle(dataGridView3.Rows[i].Cells[5].Value);
                quantity = Convert.ToInt32(dataGridView3.Rows[i].Cells[4].Value);
                tax = Convert.ToSingle(dataGridView3.Rows[i].Cells[6].Value);
                sum += quantity * (price + (price * tax));
            }

            label20.Text = sum.ToString();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        public void refreshCustomerImage()
        {
            String photoPath = textBox19.Text.Trim();
            if (photoPath != null && File.Exists(photoPath))
            {
                pictureBox1.Image = Image.FromFile(photoPath);
            }
            else {
                pictureBox1.Image = Image.FromFile("C:\\Users\\user\\Desktop\\nikos new\\SQL\\Project\\Photos\\error.png");
            }

        }

        private void bindingNavigator1_RefreshItems(object sender, EventArgs e)
        {
            refreshCustomerImage();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String openPath;
            if (openFileDialog1.ShowDialog() == DialogResult.OK) {
                openPath = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
                textBox19.Text = openPath;
                pictureBox1.Image = Image.FromFile(openPath);
                command = new SqlCommand("update CUSTOMER set Photo ='" + openPath + "' where CustomerCode=" + textBox7.Text + ";", connection);
                command.ExecuteNonQuery();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            connection.Close();
        }

        private void bindingNavigator2_RefreshItems(object sender, EventArgs e)
        {
            refreshStorageImage();
        }

        private void refreshStorageImage()
        {
            String photoPath = textBox19.Text.Trim();
            if (photoPath != null && File.Exists(photoPath))
            {
                pictureBox2.Image = Image.FromFile(photoPath);
            }
            else {
                pictureBox2.Image = Image.FromFile("C:\\Users\\user\\Desktop\\nikos new\\SQL\\Project\\Photos\\error.png");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            String openPath;
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                openPath = openFileDialog2.InitialDirectory + openFileDialog2.FileName;
                textBox23.Text = openPath;
                pictureBox2.Image = Image.FromFile(openPath);
                command = new SqlCommand("update Storage set Photo ='" + openPath + "' where ProductCode=" + textBox12.Text + ";", connection);
                command.ExecuteNonQuery();
            }
        }
    }
}
