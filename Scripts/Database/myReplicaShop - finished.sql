USE [master]
GO
/****** Object:  Database [myReplicaShop]    Script Date: 2020-08-23 6:30:19 μμ ******/
CREATE DATABASE [myReplicaShop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'myReplicaShop', FILENAME = N'C:\Users\user\Desktop\nikos new\SQL\Project\myReplicaShop.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'myReplicaShop_log', FILENAME = N'C:\Users\user\Desktop\nikos new\SQL\Project\myReplicaShop_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [myReplicaShop] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [myReplicaShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [myReplicaShop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [myReplicaShop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [myReplicaShop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [myReplicaShop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [myReplicaShop] SET ARITHABORT OFF 
GO
ALTER DATABASE [myReplicaShop] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [myReplicaShop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [myReplicaShop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [myReplicaShop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [myReplicaShop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [myReplicaShop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [myReplicaShop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [myReplicaShop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [myReplicaShop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [myReplicaShop] SET  DISABLE_BROKER 
GO
ALTER DATABASE [myReplicaShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [myReplicaShop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [myReplicaShop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [myReplicaShop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [myReplicaShop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [myReplicaShop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [myReplicaShop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [myReplicaShop] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [myReplicaShop] SET  MULTI_USER 
GO
ALTER DATABASE [myReplicaShop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [myReplicaShop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [myReplicaShop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [myReplicaShop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [myReplicaShop] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [myReplicaShop] SET QUERY_STORE = OFF
GO
USE [myReplicaShop]
GO
/****** Object:  Table [dbo].[OrderedProducts]    Script Date: 2020-08-23 6:30:19 μμ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderedProducts](
	[OrderID] [int] NOT NULL,
	[ProductCode] [int] NOT NULL,
	[Quantity] [float] NULL,
 CONSTRAINT [PK_IDCODE] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC,
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Quantity]    Script Date: 2020-08-23 6:30:19 μμ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[Quantity] as
Select ProductCode,Quantity
from OrderedProducts
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 2020-08-23 6:30:19 μμ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[CustomerCode] [int] IDENTITY(1,1) NOT NULL,
	[Surname] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[DateOfBirth] [smalldatetime] NULL,
	[Age] [int] NULL,
	[VAT] [varchar](10) NULL,
	[TaxOffice] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[Photo] [varchar](200) NULL,
	[Comments] [varchar](100) NULL,
 CONSTRAINT [PK_Code] PRIMARY KEY CLUSTERED 
(
	[CustomerCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 2020-08-23 6:30:19 μμ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[DateOfOrder] [timestamp] NOT NULL,
	[CustomerCode] [int] NULL,
	[Payment] [varchar](50) NULL,
	[PlaceOfDelivery] [varchar](50) NULL,
 CONSTRAINT [PK_OrderID] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Storage]    Script Date: 2020-08-23 6:30:19 μμ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Storage](
	[ProductCode] [int] IDENTITY(1,1) NOT NULL,
	[Kind] [varchar](50) NULL,
	[Category] [varchar](50) NULL,
	[Reserve] [int] NULL,
	[Price] [money] NULL,
	[Tax] [real] NULL,
	[Photo] [varchar](250) NULL,
 CONSTRAINT [PK_CodeStorage] PRIMARY KEY CLUSTERED 
(
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CUSTOMER] ON 

INSERT [dbo].[CUSTOMER] ([CustomerCode], [Surname], [Name], [DateOfBirth], [Age], [VAT], [TaxOffice], [Adress], [City], [PhoneNumber], [Photo], [Comments]) VALUES (1, N'Wick', N'John', CAST(N'1980-03-23T00:00:00' AS SmallDateTime), 40, N'90044221', N'D LA', N'Wellington 56', N'LA', N'142-452-1251', N'C:\Users\user\Desktop\nikos new\SQL\Project\Photos\john.png', N'')
INSERT [dbo].[CUSTOMER] ([CustomerCode], [Surname], [Name], [DateOfBirth], [Age], [VAT], [TaxOffice], [Adress], [City], [PhoneNumber], [Photo], [Comments]) VALUES (2, N'Papadopoulos', N'Spiros', CAST(N'1973-01-06T00:00:00' AS SmallDateTime), 47, N'142151255', N'A Thess', N'Heron 80', N'Thessaloniki', N'6955441122', N'', N'')
INSERT [dbo].[CUSTOMER] ([CustomerCode], [Surname], [Name], [DateOfBirth], [Age], [VAT], [TaxOffice], [Adress], [City], [PhoneNumber], [Photo], [Comments]) VALUES (3, N'Aviar', N'Juan', CAST(N'1990-03-03T00:00:00' AS SmallDateTime), 30, N'214126621', N'C Mexico', N'Arenal 62', N'Mexico City', N'142-452-1251', N'', N'')
INSERT [dbo].[CUSTOMER] ([CustomerCode], [Surname], [Name], [DateOfBirth], [Age], [VAT], [TaxOffice], [Adress], [City], [PhoneNumber], [Photo], [Comments]) VALUES (5, N'Pechlivanidou', N'Dora', CAST(N'2001-01-23T00:00:00' AS SmallDateTime), 19, N'54413235', N'Serres', N'Vasileiou 30', N'Serres', N'545114214', N'C:\Users\user\Desktop\nikos new\SQL\Project\dora.png', NULL)
INSERT [dbo].[CUSTOMER] ([CustomerCode], [Surname], [Name], [DateOfBirth], [Age], [VAT], [TaxOffice], [Adress], [City], [PhoneNumber], [Photo], [Comments]) VALUES (6, N'Vas', N'Paraskevas', CAST(N'1995-04-04T00:00:00' AS SmallDateTime), 25, N'65656565', N'B Kilkis', N'Kerasias 85', N'Kilkis', N'4544211', NULL, NULL)
INSERT [dbo].[CUSTOMER] ([CustomerCode], [Surname], [Name], [DateOfBirth], [Age], [VAT], [TaxOffice], [Adress], [City], [PhoneNumber], [Photo], [Comments]) VALUES (7, N'Kiouts', N'Yuki', CAST(N'1960-04-04T00:00:00' AS SmallDateTime), 60, N'451112241', N'Serres', N'Agias Sofias 1', N'Serres', N'23215512', N'', NULL)
INSERT [dbo].[CUSTOMER] ([CustomerCode], [Surname], [Name], [DateOfBirth], [Age], [VAT], [TaxOffice], [Adress], [City], [PhoneNumber], [Photo], [Comments]) VALUES (8, N'Mam', N'Antzi', CAST(N'1955-03-25T00:00:00' AS SmallDateTime), 65, N'5633211', N'Kordelio H', N'Milano 3', N'Athina', N'652112251', N'C:\Users\user\Desktop\nikos new\SQL\Project\Photos\john.png', N'Manager')
SET IDENTITY_INSERT [dbo].[CUSTOMER] OFF
GO
INSERT [dbo].[OrderedProducts] ([OrderID], [ProductCode], [Quantity]) VALUES (4, 1, 15)
INSERT [dbo].[OrderedProducts] ([OrderID], [ProductCode], [Quantity]) VALUES (5, 2, 5)
INSERT [dbo].[OrderedProducts] ([OrderID], [ProductCode], [Quantity]) VALUES (6, 3, 2)
INSERT [dbo].[OrderedProducts] ([OrderID], [ProductCode], [Quantity]) VALUES (7, 3, 4)
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderID], [CustomerCode], [Payment], [PlaceOfDelivery]) VALUES (4, 1, N'CARD', N'LA')
INSERT [dbo].[Orders] ([OrderID], [CustomerCode], [Payment], [PlaceOfDelivery]) VALUES (5, 2, N'CASH', N'Thessaloniki')
INSERT [dbo].[Orders] ([OrderID], [CustomerCode], [Payment], [PlaceOfDelivery]) VALUES (6, 3, N'DEPOSIT', N'Mexico city')
INSERT [dbo].[Orders] ([OrderID], [CustomerCode], [Payment], [PlaceOfDelivery]) VALUES (7, 1, N'CASH', N'Kilkis')
INSERT [dbo].[Orders] ([OrderID], [CustomerCode], [Payment], [PlaceOfDelivery]) VALUES (8, 6, N'CASH', N'Serres')
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Storage] ON 

INSERT [dbo].[Storage] ([ProductCode], [Kind], [Category], [Reserve], [Price], [Tax], [Photo]) VALUES (1, N'Chair', N'furniture', 40, 30.0000, 0.24, NULL)
INSERT [dbo].[Storage] ([ProductCode], [Kind], [Category], [Reserve], [Price], [Tax], [Photo]) VALUES (2, N'hammer', N'tool', 20, 5.0000, 0.24, N'C:\Users\user\Desktop\nikos new\SQL\Project\Photos\hammer.png')
INSERT [dbo].[Storage] ([ProductCode], [Kind], [Category], [Reserve], [Price], [Tax], [Photo]) VALUES (3, N'axe', N'furniture', 15, 4.5000, 0.24, N'C:\Users\user\Desktop\nikos new\SQL\Project\Photos\axe.png')
SET IDENTITY_INSERT [dbo].[Storage] OFF
GO
ALTER TABLE [dbo].[OrderedProducts]  WITH CHECK ADD FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[OrderedProducts]  WITH CHECK ADD FOREIGN KEY([ProductCode])
REFERENCES [dbo].[Storage] ([ProductCode])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([CustomerCode])
REFERENCES [dbo].[CUSTOMER] ([CustomerCode])
GO
/****** Object:  Trigger [dbo].[calculate_age]    Script Date: 2020-08-23 6:30:19 μμ ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[calculate_age] on [dbo].[CUSTOMER]
for insert
as
begin
	update CUSTOMER set Age = DATEDIFF(Year, DateOfBirth, GetDate())
end
GO
ALTER TABLE [dbo].[CUSTOMER] ENABLE TRIGGER [calculate_age]
GO
USE [master]
GO
ALTER DATABASE [myReplicaShop] SET  READ_WRITE 
GO
