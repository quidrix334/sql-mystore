create trigger calculate_age on CUSTOMER
for insert
as
begin
	update CUSTOMER set Age = DATEDIFF(Year, DateOfBirth, GetDate())
end