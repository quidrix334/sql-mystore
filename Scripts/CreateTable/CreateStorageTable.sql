CREATE TABLE Storage(
ProductCode int not null identity(1,1),
Kind varchar(50),
Category varchar(50),
Reserve int,
Price money,
Tax real,
Constraint PK_CodeStorage primary key (ProductCode)
);