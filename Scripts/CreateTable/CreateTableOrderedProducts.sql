create table OrderedProducts (
OrderID int not null,
ProductCode int not null,
Quantity float,
Constraint PK_IDCODE primary key(OrderID,ProductCode)
);