CREATE TABLE CUSTOMER(
CustomerCode INT not null identity(1,1),
Surname varchar(50),
Name varchar(50),
DateOfBirth SMALLDATETIME,
Age INT,
VAT varchar(10),
TaxOffice varchar(50),
Adress varchar(50),
City varchar(50),
PhoneNumber varchar(50),
Photo varchar(200),
Comments varchar(100),
Constraint PK_Code primary key (CustomerCode)
);