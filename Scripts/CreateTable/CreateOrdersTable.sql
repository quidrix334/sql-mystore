create table Orders (
OrderID int not null identity(1,1),
DateOfOrder timestamp,
CustomerCode int,
Payment varchar(50),
PlaceOfDelivery varchar(50),
Constraint PK_OrderID primary key(OrderID)
);